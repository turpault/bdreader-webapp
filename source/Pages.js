/*
enyo.kind({
  kind:"ImageCarousel",
  name:'pagesUI',
  lowMemory:true,

  display: function(urls) {    
    this.setImages(urls);
    this.setIndex(0);
    this.reflow();
    
  },
  nextImage: function() {
    this.next();
  },
  previousImage:function() {
    this.previous();
  }});
*/

enyo.kind({
  kind:"Panels",
  name:'pagesUI',
  margin:20,
  arrangerKind:"LeftRightArranger",
  urls:[],
  handlers:{
    onTransitionFinish:"update"
  },
  events: {
    onPageChanged:""
  },
  display: function(urls) {    
    this.getPanels().slice().forEach(function(panel) { panel.destroy();});
    var panel=this;
    urls.forEach(function(url) {
      var page = panel.createComponent({kind:'FittableRows', src_data:url});
      page.render();      
    });
    this.reflow();
    this.setIndex(0);
    this.update();
  },
  update: function() {
    console.log("Updating - current index is " + this.getIndex());
    var panels = this.getPanels(),
        panel = this,
        index = this.getIndex();
    for(var i = 0 ; i < panels.length ; i++) {
      if(Math.abs(index - i)<2) {
        if(!panels[i].image) {
          console.log('Creating page ' + i + ' url=' + panels[i].src_data);
          var page = panels[i].createComponent({kind:"ImageView", fit:true, src:panels[i].src_data});
          page.render();
          page.reflow();
          panels[i].reflow();
          panels[i].image = page;
          this.reflow();
        }
      } else {
        if(panels[i].image) {
          console.log('Destroying page ' + i);
          panels[i].image.destroy();
          delete panels[i].image;
        }
      }
    } 
    console.log('Last index is ' + this.lastIndex);
    if(this.lastNotifiedIndex!=index) {
      this.doPageChanged({index:index}); 
      this.lastNotifiedIndex = index;
    }
  },
  showPage:function(index) {
    console.log("Showing page " + index);
    this.setIndex(index);
  },
  nextImage: function() {
    this.next();
  },
  previousImage:function() {
    this.previous();
  }
});

