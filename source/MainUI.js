enyo.kind({
  name:"mainUI",
  kind:"FittableRows",
  components:[
    {
      kind: enyo.Signals, 
      onkeypress: "handleKeyPress",
      onkeydown: "handleKeyDown",
      onholdpulse: "onholdPulse"
    },  {  
      kind: "onyx.MoreToolbar",
      name: "toolbar",
      components:[
        {
          kind:"onyx.Button",
          content:"X",
          ontap:"back"
        },
        {
          kind:'onyx.Button',
          ontap:'previousImage',
          content:'<'
        }, 
        {
          kind: "onyx.PickerDecorator", 
          components: [
            {}, //this uses the defaultKind property of PickerDecorator to inherit from PickerButton
            {kind: "onyx.IntegerPicker", min: 1, max: 1, value: 1, name:"pageNumber", onSelect: "pageSelected"}
          ]
        },{
          kind:'onyx.Button',
          ontap:'nextImage',
          content:'>'
        } , 
        {kind: "onyx.PickerDecorator", components: [
  				{},
	  			{kind: "onyx.Picker", onSelect: "resizeSelected", components: [
		  			{content: "Taille Originale", _dimensions:undefined, active: true},
			  		{content: "Taille de l'ecran", _dimensions:{width:-1,height:-1}},
				  	{content: "1000x1000 Pixels", _dimensions:{width:1000,height:1000}}
				  ]}
				]},
        {content:"", name:"title"},        //{name: "status", content:"ready"},
        {fit:true},
        {
          kind:"onyx.Button",
          content:"Plein ecran",
          ontap:"hideToolbar"
        }
      ]
    },{ 
      name:"pages",
      kind:"pagesUI",
      fit:true
    }
  ],
  // Can send the openPanel event
  events: {
    onOpenPanel:"",
    onNotification:"" ,
    onReturnToMain:""
  },
  
  // can process the page changed event
  handlers:{
    onPageChanged:"pageChanged"
  },
  pageChanged: function(inSender, inEvent) {
    console.log('Page changed : ' + inEvent.index);
    this.$.pageNumber.setValue(inEvent.index+1);
  },
  pageSelected: function(inSender, inEvent) {
    this.$.pages.showPage(parseInt(inEvent.selected.content));
  },  
  openFile:function(file){    
    var title=file.text;
    this.$.title.setContent(title);
    this.isLocal = (file.urls != undefined);
    if(file.urls) {
      this.doNotification({title:title, message:"Opened " +file.urls.length + " pages"});
      this.display(file.urls);
    } else {
      var that=this;
      that.doNotification({title:title, message:"Opening file..."});
      MODEL.getPages(file.path,function(err,result) {
        var urls = result.data;
        if(err)
          that.doNotification({title:title, message:"Error :" + err});
        else {
          that.doNotification({title:title, message:"Opened " +urls.length + " pages"});
          that.display(urls);
        }
      });    
    }
  },
  display:function(urls) {
    // Sort pages based on file names
    /*
    var numbers=/[0-9]+/;
    var sortPages = function(a,b) {
      // a and b are urls. Extract file names
      var fa=a.split('/').pop();
      var fb=b.split('/').pop();
      // get only the numbers in that file
      var na=parseInt(fa.match(numbers),10);
      var nb=parseInt(fb.match(numbers),10);
      var result=0;
      if(na!=nb) 
        result=na<nb?-1:1;
      else
        result=a<b?-1:1;
      return result;
    };
    urls.sort(sortPages);
    */
    this.$.pageNumber.setMax(urls.length);
    this.urls = urls;
    this.updatePages();
  },
  onShow:function() {},
  updatePages:function() {
    var urls=[];
    if(this.imageDimensions && this.isLocal) {
      this.doNotification({title:'Resizing ignored because the album is local'});
    }
    if(this.imageDimensions && !this.isLocal) {
      console.log(this.imageDimensions);
      var bounds = this.getBounds(),
          width=this.imageDimensions.width || bounds.width,
          height=this.imageDimensions.height || bounds.height;
      this.doNotification({title:"Custom dimensions chosen ("+width+","+height+")"});
      this.urls.forEach(function(url) { 
        urls.push(MODEL.config.serverURL+'/resize?url='+encodeURIComponent(MODEL.config.serverURL+url)+'&width='+width+'&height='+height);
      });
    } else if(!this.isLocal) {
      this.urls.forEach(function(url) { urls.push(MODEL.config.serverURL+url); });
    } else  {
      urls = this.urls;
    }
    this.$.pages.display(urls);
  },
  nextImage:function(inSender, inEvent) {
    this.$.pages.nextImage();
  },
  previousImage:function(inSender, inEvent) {
    this.$.pages.previousImage();
  },
  resizeSelected: function(inSender, inEvent) {    
    this.imageDimensions=inEvent.originator._dimensions;
    this.doNotification({title:'Resizing mode', message:'Resizing to '+inEvent.originator.getContent()});
    console.log(this.imageDimensions);
    this.updatePages();    
  },  
  showToolbar:function(inSender, inEvent) {
    this.$.toolbar.show();
  },
  hideToolbar:function(inSender, inEvent) {
    this.$.toolbar.hide();
  },
  back:function(inSender,inEvent) {
    this.doReturnToMain();
  },
  onShowPanel:function(inSender, inEvent) {
    console.log('Returning to main panel');
    this.doOpenPanel({panel:inSender.name});
  },
  handleKeyPress:function(inSender, inEvent) {
    if(inEvent.keyCode==32) {
      this.nextImage();
    }
  },
  handleKeyDown:function(inSender, inEvent) {
    if(inEvent.keyCode==39) {
      this.nextImage();
    } else if(inEvent.keyCode==37) {
      this.previousImage();
    }
  }  ,
  onholdPulse: function(inSender, inEvent) {
    console.log("holdPulse");
    if(inEvent.holdTime>1000) {
      this.showToolbar();
    }
  }
});
