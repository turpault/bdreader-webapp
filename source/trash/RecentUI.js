enyo.kind({
  name: "recentUI",   
  kind: "FittableRows", 
  narrowFit: false,
  fit:true,
  components:[
    {  
      kind: "onyx.MoreToolbar",
      components:[
        { content: "Recently added files"},
        { fit:true}, 
        {
          kind:'onyx.Button',
          ontap:'close',
          content:'Close',
          unmoveable:true
        }
      ]
    },{  
      kind: "files",
      fit: true,      
      name:'recentFiles',
    }  
  ],

  onShow:function() {
    var that = this;
    methods.getRecentFiles(function(err, results) {
      if(err) { 
      	console.log("error - displaying reason");
      } else {		
        console.log("Got " + results.data.length + " recent files");
        that.$.recentFiles.renderList(results.data);
			}
    });
  }  ,
  events:{
    onClose:""
  },
  close:function() { this.doClose();}  

});
