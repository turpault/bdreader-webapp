enyo.kind({
  name: "configUI",   
  kind: "FittableRows", 
  fit: true,
  narrowFit: false,
  classes: "onyx",
  components:[
    {
      kind: enyo.Signals, 
      onkeypress: "handleKeyPress",
      onkeydown: "handleKeyDown"
    }, {  
      kind: "onyx.MoreToolbar",
      name: "toolbar",
      components:[
        {
          content:'Path:'
        }, {
          kind:'Button',
          ontap:'close',
          content:'Close',
          unmoveable:true
        }
      ]
    },{  
      kind: "enyo.Control",
      fit: true,
      name:'pane',
      style:"background:grey",
      narrowFit: false
    }  
  ],
  onShow:function() {}
  
});
