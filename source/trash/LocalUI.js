enyo.kind({
  name: "localUI",   
  kind: "FittableRows", 
  narrowFit: false,
  fit:true,
  components:[
    {  
      kind: "onyx.MoreToolbar",
      components:[
        { content: "Recently added files"},
        { fit:true}, 
        {
          kind:'onyx.Button',
          ontap:'close',
          content:'Close',
          unmoveable:true
        }
      ]
    },{  
      kind: "files",
      fit: true,      
      name:'localFiles',
    }  
  ],

  onShow:function() {
    var that = this;
    methods.getLocalFiles(function(err, results) {
      if(err) { 
      	console.log("error - displaying reason");
      } else {		
        console.log("Got " + results.data.length + " recent files");
        that.$.localFiles.renderList(results.data);
			}
    });
  }  ,
  events:{
    onReturnToMain:""
  },
  close:function() { this.doReturnToMain();}  

});
