enyo.kind({
  name:"PanelButton",
  classes: "onyx",
  kind:"onyx.Button",
  style:"height:80px",
  components:[
    {
      kind:"Image",
      src:"",
      name:'icon'
    },{
      tag:"br"
    },
    {
      content:"",
      name:'text'      
    }
  ],
  create:function(options) {
    this.inherited(arguments);
    this.$.icon.setSrc(options.src);
    this.$.text.setContent(options.text);
  }  
});

// Top level controls. Contains popups, and notification bars, that must be displayed above anything else
enyo.kind({
  name: "BDReader",
  classes: "onyx",
  kind:"FittableRows",  
  fit:true,   
  components:[
    {
      kind:"Panels",
      name:"panels",
      fit:true, 
      arrangerKind:"SladeArranger",
      draggable:false,
      components:[
        {kind:"Menu", name:"menu"},
        {kind:"mainUI", name:"mainPanel"}
      ]
    },
    {kind: "Notification", name: "notif"},
    {
      kind: "onyx.Popup", 
      centered: true, 
      modal: true, 
      name:"feedbackPopup",
      floating: true, 
      components: [
        {content:"You opinion of the app ?"},
        {kind: "onyx.InputDecorator", components: [
          {name:"feedbackText", kind: "onyx.Input"}
        ]},
        {tag: "br"},
        {kind: "onyx.Button", content: "Submit", ontap:"submitFeedback"}
      ]
    }
  ],
  handlers: {
    onFileSelected:"fileSelected",    
    onReturnToMain:"returnToMenu",
    onReturnToBD:"returnToBD",
    onHoldPulse:"holdPulse",
    onNotification:"notification",
    onAskFeedback:"askFeedback"
  },
  fileSelected:function(inSender,params) {
    console.log("Selected file " + params.file.path);
    this.$.panels.setIndex(1);
    this.$.mainPanel.openFile(params.file);
  },
  returnToMenu:function() {
    this.$.panels.setIndex(0);
  },
  returnToBD:function() {
    this.$.panels.setIndex(1);
  },
  notification: function(inSender, inEvent) {
    this.$.notif.sendNotification({
          title: inEvent.title,
          message: inEvent.message,
          icon: inEvent.icon || 'assets/icon-alert-big.gif',
          theme: "notification.MessageBar",
          stay: false,
          duration: 2
        }, inEvent.callback);
  },
  askFeedback:function() {
    this.$.feedback.show();
  }  
});

// Menus controllers
enyo.kind({
  name: "Menu",
  classes: "onyx",
  kind:"FittableRows",  
  fit:true,    
  components:[
    {
      kind:"Panels",
      name:"panels",
      draggable:false,
      fit:true,
      components:[
        {
          kind:"filesUI", name:"serverPanel"
        },{
          kind:"filesUI", name:"recentPanel"
        },{
          kind:"filesUI", name:"downloadedPanel"
        },{
          kind:"filesUI", name:"favoritesPanel"
        },{
          kind:"mainUI", name:"mainPanel"
        },{
          kind:"settingsUI", name:"settingsPanel"
        }
      ]
    }, {  
      kind: "onyx.MoreToolbar",
      name: "PanelSelector",
      layoutKind: "FittableColumnsLayout",
      components:[
        {kind:"onyx.RadioGroup", onActivate:"tabActivated", controlClasses: "onyx-tabbutton",components:[
          { kind: "PanelButton", src: "assets/globe_48.png", pane:"server",    active:true, name:'serverBtn' , text:"Serveur"},
          { kind: "PanelButton", src: "assets/inbox_48.png", pane:"downloaded", name:'downloadedBtn', text:"Local"},
          { kind: "PanelButton", src: "assets/heart_48.png", pane:"recent",    name:'recentBtn' , text:"Recent"},
          { kind: "PanelButton", src: "assets/star_48.png",  pane:"favorites", name:'favoritesBtn', text:"Favoris"},
          { kind: "PanelButton", src: "assets/gear_48.png",  pane:"settings",  name:'settingsBtn' , text:"Parametres"}
        ]},
        { kind: "PanelButton", src: "assets/comment_check_48.png",  ontap: "comment", text:"Opinion"},
        { kind: "PanelButton", src: "assets/wallet_48.png",  ontap: "back", text:"Lecteur"}
      ]
    }
  ],
  events: {
    onReturnToBD:"",
    onAskFeedback:""
  },
  create:function() {
    this.inherited(arguments);
    this.$.serverPanel.openServer();
    this.$.recentPanel.openRecent();
    this.$.downloadedPanel.openDownloaded();
    this.$.favoritesPanel.openFavorites();
    this.$.downloadedBtn.setShowing(MODEL.hasFiles());
    this.$.favoritesBtn.setShowing(MODEL.hasFiles());
//    this.showPanel("server");
  },      
  reload:function() {
    console.log('Reloading');
    window.location.reload();
  },
  tabActivated:function(inSender, inEvent) {
    if (inEvent.originator.getActive()) {      
      this.showPanel(inEvent.originator.pane+"Panel");
    }
  },
  back:function(inSender, inEvent) {
    this.doReturnToBD();
  },
  comment:function(inSender, inEvent) {
    this.doAskFeedback();
  },
  showPanel:function(name) {
    console.log("Opening panel " + name);
    var panels = this.$.panels.getPanels();
    for(var i=0;i<panels.length;i++) {      
      if(panels[i].name == name) {
        this.$.panels.setIndex(i);
        panels[i].onShow();
        break;
      }
    }
  }

  
});



var app = {
  // Application Constructor
  initialize: function(options) {   
    app.host=options.host;
    app.enable_bugsense=options.enable_bugsense;
    
    // Small utility to workaround the lack of the isArray function
    Array.prototype.isArray = true;
    //console.log('Initializing');
    
    if(options.log === false) {
      console.log('No logs');
      console.log=enyo.nop;
    }

    if(options.host==="phonegap") {
      console.log('Phonegap build');
      document.addEventListener("deviceready", app.onDeviceReady, false);
    } else {
      app.onDeviceReady(); //this is the browser
    }
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'app.receivedEvent(...);'
  onDeviceReady: function() {
          
    console.log('Device is ready');
    if(app.enable_bugsense)
      var bugsense = new Bugsense( { apiKey: 'a6ed8af6' } );
    if(app.host==="phonegap") {
      /*window.tf = cordova.require("cordova/plugin/testflightsdk");
      tf.takeOff(
        function() {console.log('testflight initialized');},
        function() {console.log('testflight init failed');},
        'dbdfbf2a-2f01-488e-aa29-5dba1b4f3865');*/
    }
      
    MODEL.init(function() {      
      new BDReader().renderInto(document.body);     
    }); 
  }
};
