enyo.kind({
  name:'file_as_row', 
  style:"margin:5px;",
  ontap: "rowTap",
  layoutKind:'FittableColumnsLayout',
  components:[
    {
      kind: "onyx.Button", 
      name:"download", 
      style:"margin-right:5px;height:36px;width:45px;padding:0px;",
      ontap:"downloadTapped", components: [{tag: "img", attributes: {src:"assets/download.png"}}]
    },{
      kind: "onyx.Button", 
      name:"favorite", 
      style:"margin-right:5px;height:36px;width:45px;padding:0px;",
      ontap:"starTapped", components: [{tag: "img", name:'star',attributes: {src:"assets/star.png"}},
                                       {tag: "img", name:'unstar', attributes: {src:"assets/star-unselected.png"}}]
    },{
      kind: "onyx.Button", 
      name:"trash", 
      style:"margin-right:5px;height:36px;width:45px;padding:0px;",
      ontap:"trashTapped", components: [{tag: "img", attributes: {src:"assets/trash_recyclebin_full.png"}}]
    },        
    {
      name:'item', 
      content:'...'
    }
  ],
  setup:function(item) {
    var hasFiles=MODEL.hasFiles();
    this.$.download.setShowing(hasFiles &&  (item.type != "folder") && (item.urls==undefined));
    this.$.trash.setShowing(hasFiles && item.urls);
    this.$.star.setShowing(hasFiles && MODEL.isStarred(item));
    this.$.unstar.setShowing(hasFiles &&  !MODEL.isStarred(item));
    this.$.favorite.setShowing(hasFiles);
    this.$.item.setContent(item.text);        
  }   
});




enyo.kind({
  name: "fileItem",
  kind: "Button",
  components:[
    {
      kind:"Image",
      name:"img"
    },
    {tag:"br"},
    {name:"label"}
  ],
  events:{
    onSelectedFolder:""
  }
  });
  
enyo.kind({
  name: "files",
  narrowFit: false,
  kind: "List", 
  handlers: {
    onSetupItem: "setupItem",
    },
  fit:true,
  style:"min-width:250px;width:100%;",
  fixedHeight:true,
  draggable:false,
  enableSwipe:false,
  components: [ 
    {kind: 'file_as_row', name:'box'},
    {name: "divider", classes: "file-list-divider"}
  ],
  downloadTapped:function(inSender,inEvent) {
    var album=this.data[inEvent.index];
    MODEL.saveAlbum(album, 
      enyo.bind(this, function(err,message) {
        console.log(message);
        this.doNotification({title:album.text, message:message});
      })
    );
    return true;
  },
  trashTapped:function(inSender, inEvent) {
    var album=this.data[inEvent.index];
    MODEL.trashAlbum(album, 
      enyo.bind(this, function(err,message) {
        console.log(message);
        this.doNotification({title:album.text, message:message});
        this.data.splice(inEvent.index,1);
        this.setCount(this.data.length);
        this.reset();
      })
    );
    return true;
  },
  starTapped:function(inSender, inEvent) {
    var album=this.data[inEvent.index];
    var currentValue = MODEL.isStarred(album);
    var newValue= !currentValue;
    MODEL.starAlbum(album, newValue,
      enyo.bind(this, function(err,message) {
        console.log(message);
        this.doNotification({title:album.text, message:message});
        if(!err) {
          this.renderRow(inEvent.index);
        }
      })
    );
    return true;
  },
    
  events: {
    onFileSelected:"",
    onFolderSelected:"",
    onNotification:""
  },
  setupItem: function(inSender, inEvent) {
    // this == list
    var item = this.data[inEvent.index];
    this.$.box.setup(item);
    this.$.box.addRemoveClass("onyx-selected", inEvent.selected);      
  },
  rowTap: function(inSender, inEvent) {
    var item = this.data[inEvent.index];
    if(item.type=="folder") {    
      this.doFolderSelected({folder:item, index:this.index});
    } else {
      this.doFileSelected({file:item, index:this.index});
    }        
  },
  renderList: function(data) {
    this.data = data;
    this.setCount(this.data.length);
    this.reset();
    this.refresh();
    this.reflow();
  }  
});
  

enyo.kind({
  name:'file_as_cell', 
  style:"width:120px;height:210px;padding:5px;",
  kind:"onyx.Button",
  handlers : {
    ontap: "ontap"
  },
  components:[
    {
      style:"width:100px;margin-left:auto;margin-right:auto;",
      kind: "Image",
      name: "cover"
    },{tag:'br'},
    {
      style:"margin-left:auto;margin-right:auto;text-overflow: ellipsis;",
      name: "title"
    }
  ],
  create:function(options) {    
    this.inherited(arguments);
    if("item" in options)
      this.setup(options.item);
  },
  setup: function(item)
  {
    console.log('Created ' + item.text);
    this.item = item;    
    this.$.cover.setSrc(MODEL.config.serverURL+this.item.thumburi);
    this.$.title.setContent(this.item.text);
  },
  events: {
    onFileSelected:"",
    onFolderSelected:""
  },
  ontap: function(inSender, inEvent) {
    console.log('Tapped on ' + this.item.text);
    if(this.item.type=='folder') 
      this.doFolderSelected({folder:this.item, index:this.index});
    else if(this.item.type=='file') 
      this.doFileSelected({file:this.item, index:this.index});
  }
});

enyo.kind({
  name: "filegrid",
//  narrowFit: false,
//  arrangerKind:"GridArranger",
  kind:"enyo.Scroller",
//  fit:true,
//  style:"min-width:250px;width:100%;",
//  draggable:false,
//  enableSwipe:false,
  components: [   ],
  events: {
    onFileSelected:"",
    onFolderSelected:"",
    onNotification:""
  },
  renderList: function(data) {
    if(this.components)
      this.components.forEach(function(component) { component.destroy();});
      
    this.components = this.createComponents(
      data.map(function(obj) {
        return {
          kind:"file_as_cell",
          item:obj,
          index:this.index
        }
      })
    );
    this.components.forEach(function(component) {
      component.render();
      console.log('Component : ' + component.hasNode());
    });
    this.reflow();
  }
});


enyo.kind({
  name: "filegrid-2.3",
//  narrowFit: false,
//  arrangerKind:"GridArranger",
  kind:"enyo.GridList",
//  fit:true,
//  style:"min-width:250px;width:100%;",
//  draggable:false,
//  enableSwipe:false,
  onSizeupItem: "sizeupItem",
  onSetupItem: "setupItem", 
  itemMinWidth: 160, 
  itemSpacing: 2, 
  components: [ {name:"box", kind: "file_as_cell"} ],
  events: {
    onFileSelected:"",
    onFolderSelected:"",
    onNotification:""
  },
  sizeupItem: function(inSender, inEvent) {
    var item = this._data[inEvent.index];
      // fixed size
      inSender.setItemWidth(100);
      inSender.setItemHeight(220);
  },
  setupItem: function(inSender, inEvent) {
      var item = this.data[inEvent.index];
      this.$.box.setup(item);
  },
  rowTap: function(inSender, inEvent) {
    console.log('Tapped on index' + inEvent.index);
    var item = this.data[inEvent.index];
    if(item.type=="folder") {    
      this.doFolderSelected({folder:item, index:this.index});
    } else {
      this.doFileSelected({file:item, index:this.index});
    }        
  },
  renderList: function(data) {
    this.data = data;
    this.setCount(this.data.length);
    this.reset();
    this.refresh();
    this.reflow();
  }    
});
