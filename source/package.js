enyo.depends(
  "$lib/layout",
  "$lib/onyx",  // To theme Onyx using Theme.less, change this line to $lib/onyx/source,
  //"Theme.less",  // uncomment this line, and follow the steps described in Theme.less
  "more-arrangers",
  "enyo2-lib/notification",
  "async/async.js",
  "enyo-extras",
  "tools",
  
  "ip.js",
  "Model.js",
  "Files.js",
  "FilesUI.js",
  "SettingsUI.js",
  "MainUI.js",
  "Pages.js",
  "App.css",
  "App.js"
);
