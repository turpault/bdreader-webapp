
enyo.kind({
  name: "settingsUI",   
  kind: "FittableRows", 
  fit: true,
  narrowFit: false,
  classes: "onyx",
  components:[
    {  
      kind: "onyx.MoreToolbar",
      name: "toolbar",
      components:[
        {
          content:"Settings"
        },
        {fit:true},
        {
          kind:'onyx.Button',
          name:'close',
          ontap:'close',
          content:'Close'
        }
      ]
    } , 
    {classes: "onyx-sample-divider", content: "Server's address"},
    {kind: "onyx.InputDecorator", components: [
        {kind: "onyx.Input", style: "min-width: 300px;", value: MODEL.config.serverURL, onchange:"inputChanged"}
      ]
    },
    {tag: "br"},
    {classes: "onyx-sample-divider", content: "Database"},
    {kind:"onyx.Button", content:"Rescan recent files", ontap:"rescanrecent"},
    {tag: "br"},
    {kind:"onyx.Button", name:'purge', content:"Purge local files", ontap:"purgelocal"},
    {tag: "br"},
    {classes: "onyx-sample-divider", content: "Viewer"},
    {classes: "onyx-toolbar-inline", components: [
      {content: "Pages to prefetch", classes: "onyx-sample-label"},
      {kind: "onyx.PickerDecorator", components: [
        {style: "min-width: 60px;"},
        {kind: "onyx.IntegerPicker", min: 1, max: 5, value: 2}
      ]}
    ]}  
  ],
  events:{
    onReturnToMain:"",
    onNotification:''
  },
  create:function() {
    this.inherited(arguments);
    this.$.purge.setShowing(MODEL.hasFiles());
  },
  rescanrecent:function(inSender, inEvent) {
    MODEL.rescan();
    this.doNotification({title:'Restarted recent scan'});
  },
  purgelocal:function(inSender, inEvent) {
    MODEL.clearFiles();
    this.doNotification({title:'Deleted all local files'});
  },
  inputChanged:function(inSender, inEvent) {
  },
  
  onShow:function() {},
  close:function() { this.doReturnToMain();}  
});
