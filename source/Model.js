
var MODEL = {
  // Data members
  local_albums:[],
  starred_albums:{},  
  config:{
    serverURL:"http://"+ip+":8888",
    initialPath:""
  },
  
  save_progress:{
    count:0,
    total:0
  },
  
  notificationSink:function(message) {console.log(message);} ,
  
  STORAGE:'storage',
  
  clearFiles:function() {
    MODEL.starred_albums={};
    MODEL.local_albums=[];
    MODEL.write('starred_albums');
    MODEL.write('local_albums');
    MODEL.storage && MODEL.storage.removeRecursively(
      function success() {
        MODEL.createAlbumsStorage(function(err) {
        });
      },
      function error() {
        this.notificationSink('Certains fichiers n\'ont pu être effacés.');
      }
    );
  },
  // Initialization
  init:function(cb) {    
    function onSuccess(fs) {
      MODEL.fs = fs;

      console.log("Reading config & local files");
      // Read local FS data (catalog, config)
      async.parallel([
        function readLocalAlbums(cb_async) {
          MODEL.read('local_albums', function(err, contents) {
            cb_async(null);
          });
        },
        function readConfig(cb_async) {
          MODEL.read('config', function(err, contents) {
            cb_async(null);
          });
        },
        function readStarred(cb_async) {
          MODEL.read('starred_albums', function(err, contents) {
            cb_async(null);
          });
        },
        function create_albums_storage(cb_async) {
          console.log('Creating storage for albums');
          MODEL.createAlbumsStorage(function(err) {
            cb_async(null);
          });
        }
      ], function(err) {
        cb(null);
      });
    };
    
    function onError(error) {
      console.log("Could not retrieve filesystem : " + JSON.stringify(error));
      cb(error);
    };
    
    // Get filesystem
    if('webkitRequestFileSystem' in window)
    {
      console.log("Chrome");
      window.requestFileSystem = window.webkitRequestFileSystem;
      navigator.webkitPersistentStorage.requestQuota( 10*1024*1024, 
        function(grantedBytes) {
          window.requestFileSystem(window.PERSISTENT, grantedBytes, onSuccess, onError);
        },
        onError
      );
    } else if('moz_requestFileSystem' in window) {
      console.log("Mozilla");
      //firefox
      window.requestFileSystem = window.moz_requestFileSystem;
      window.requestFileSystem(window.PERSISTENT, 10*1024*1024, onSuccess, onError);
    } else if(app.host === "phonegap") {
      console.log("Phonegap");
      // phonegap      
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess, onError);
    } else {
      console.log("Something else");
      cb(null);
    }
  },

  // Ajax Calls
	serve:function(path,options,cb) {
    new enyo.Ajax({url:MODEL.config.serverURL+path}).
    	response(function(sender,response) {
    	  console.log(path+":"+response); 
    		console.log(response); 
    	  return cb(null, response); 
    	}).
    	error(function(sender,response) { 
    	  console.log(path+":"+response);
    		if(response==0) 
    			response=-1;
    		console.log(response); 
    		return cb(response);}).
    	go(options);
	},		
  getPages:function(path, cb) {
  	return MODEL.serve("/pages",{path:path, version:2},cb);
  },
  getFiles:function(path,cb) {
  	return MODEL.serve("/dir",{path:path, version:2},cb);
  },
  getFilesAndFolders:function(path,cb) {
  	return MODEL.serve("/ls",{path:path, version:2},cb);
  },
  getRecentFiles:function(cb) {
  	return MODEL.serve("/recent",{version:2},cb);
  },
  rescanRecentFiles:function(cb) {
  	return MODEL.serve("/rescanrecent",{version:2},cb);
  },
  
  isStarred:function(album) {
    return MODEL.starred_albums[album.text] != undefined;
  },
  // Filesystem operations  
  hasFiles: function() {
    return MODEL.fs != undefined;
  },
  // persistent datasets
  read:function(type, cb) {
    return MODEL.readFile(type+".json", function(err, result){
      if(!err)
        MODEL[type]=result;
      cb && cb(err,result);
    });
  },
  write:function(type, cb) {
    return MODEL.writeFile(MODEL[type], type+".json", function(err, result){
      cb && cb(err,result);
    });
  },    
  readFile:function(file,cb) {
    // Retrieve 'catalog' file, and extract the albums
    MODEL.fs.root.getFile(
      file, 
      {}, 
      function gotFileEntry(fileEntry){
        console.log("readFile:Got fileentry");
        fileEntry.file(
          function success(file) {
            console.log('Got file... ' + file);
            var reader = new FileReader();
            reader.onloadend = function(e) {
              console.log(e.target.result);
              var result = JSON.parse(e.target.result);
              cb(null, result);
            };
            reader.readAsText(file);
          },
          function fail(e) {
            console.log(e);
            cb(e);
          }
        );
      },
      function noFileEntry(error) {
        console.log('Could not get file entry');
        console.log(error);
        cb(error);
      }
    );
  },
  writeFile:function(contents, file, cb) {
    // Save the local catalog
    console.log(contents);
    MODEL.fs.root.getFile(
      file, 
      {create:true}, 
      function gotFileEntry(fileEntry){
        MODEL.fs.root.getFile(
          file, 
          {create:true}, 
          function gotFileEntry(fileEntry){
            console.log("saveFile:Got fileEntry");
            console.log(fileEntry.createWriter);
            fileEntry.createWriter(
              function(fileWriter) {
                console.log('Got filewriter');
            
                fileWriter.onwriteend = function(e) {
                  console.log('saveFile:Write completed.');
                };

                fileWriter.onerror = function(e) {
                  console.log('saveFile:Write failed: ' + e.toString());
                };

                var data = JSON.stringify(contents);
                console.log(data);
                fileWriter.write(data); 
                cb && cb(null);
              },
              function(error) {
                console.log('Error while getting filewriter');
                console.log(error);
                cb && cb(error);
              }
            );
          },
          function(error) {
            console.log(error);
            cb && cb(error);
          }         
        );        
      },
      function(error) {
        console.log(error);
        cb && cb(error);
      }
    );
  },
  createAlbumsStorage:function(cb) {
    console.log('Creating storage for albums');
    MODEL.fs.root.getDirectory(MODEL.STORAGE,{create:true},
      function success(entry) {
        MODEL.storage = entry;
        cb(null);
      },
      function failed(err) {
        console.log('Could not create storage folder' + err);
        cb(err);
      }
    );
  },
  getSaveProgress:function() {
    return MODEL.save_progress;
  },
  // Get/Save album in local storage
  getLocalFiles:function(cb) {
    cb(null,MODEL.local_albums);
  },
  getStarredFiles:function(cb) {
    var starred = [];
    for(var f in MODEL.starred_albums) {
      starred.push(MODEL.starred_albums[f]);
    }
    cb(null,starred);
  },
  saveAlbum:function(entry, cb) {
    console.log(entry);
    async.waterfall([
      function get_urls(cb_get_urls) {
        console.log('Getting urls');
        MODEL.getPages(entry.path, cb_get_urls);
      },
      function createFolder(result, cb_create_folder) {
        console.log(entry.text.match(/[0-9a-zA-Z]+/g));
        var directoryName=entry.text.match(/[0-9a-zA-Z]+/g).join('-');
        console.log('Creating folder ' + directoryName);
        MODEL.storage.getDirectory(
          directoryName,
          {create:true},
          function success(dirEntry) {
            entry.folder=dirEntry.fullPath;
            console.log('Folder full path : ' + entry.folder);
            cb_create_folder(null,{result:result, dir:entry.folder});
          },
          function failed(error) {
            console.log(error);
            cb_create_folder(error);
            cb && cb('Error', error);
          }
        )
      },
      function download_files(dir_and_files, cb_download_files) {
        console.log(dir_and_files);
        cb(null, 'Starting download of ' + dir_and_files.result.data.length + ' files');
        async.map(dir_and_files.result.data,function(url, cb_downloaded) {
          console.log(url);
          MODEL.downloadFile(dir_and_files.dir, MODEL.config.serverURL+url, cb_downloaded);
        },cb_download_files);
      },
      function modify_and_save_catalog(changed_urls, cb_modified) {
        cb && cb(null, '' + changed_urls.length + ' files saved');
        console.log('Saving catalog');
        entry.urls=changed_urls;
        MODEL.local_albums.push(entry);
        MODEL.write('local_albums',function saved(err) {        
          cb_modified(undefined);
        });
      }
    ], 
    function completed(err) {
      cb && cb(null, 'Completed');
    });
  },
  starAlbum:function(entry, starValue, cb) {
    // Add album to list of favorites
    if(starValue)
      MODEL.starred_albums[entry.text]=entry;
    else
      delete MODEL.starred_albums[entry.text];
    MODEL.write('starred_albums');
    cb && cb(null, 'Starred album ' + entry.text);
    return true;
  },
  trashAlbum:function(entry, cb) {
    // Delete local album
    MODEL.storage.getDirectory(entry.folder,
      {},
      function success(entry) {
        entry.removeRecursively(
          function success(parent) {
            for(var i=0;i<MODEL.local_albums.length;i++) {
              if(MODEL.local_albums[i].text==entry.text) {
                MODEL.local_albums.splice(i,1);                
                MODEL.write('local_albums');
                break;
              }
            }
            cb && cb(null, 'Removed ' + entry.text);
          },
          function failed(err) {
            cb&&cb(err, 'Cannot find images for album ' + entry.text);
          }
        );
      },
      function failed(err) {
        cb && cb(err, 'Cannot remove files ' + err);
      }
    );
  },
  downloadFile:function(directoryName, url, cb){
    console.log('Downloading ' + url + ' in folder ' + directoryName);
    MODEL.save_progress.total++;

    var localFilePath=decodeURIComponent(url.split('/').pop()).match( /[0-9a-zA-Z]+/g ).join('') ;
    var fileTransfer = new FileTransfer();

    fileTransfer.download(
      url,
      directoryName + '/' + localFilePath,
      function(theFile) {
        console.log("download complete: " + theFile.toURL());
        MODEL.save_progress.count++;
        cb(undefined, theFile.toURI());
      },
      function(error) {
        MODEL.save_progress.count++;
        console.log("download error source " + error.source);
        console.log("download error target " + error.target);
        console.log("upload error code: " + error.code);
        cb(error);
      }
    );
  },
};

    

