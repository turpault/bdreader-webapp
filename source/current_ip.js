var options = {
  hostname: 'ipecho.net',
  path : '/plain'
}, 
  http= require('http'),
  fs = require('fs'),
  req = http.request(options, function(res) {
    res.on('data', function(chunk) {
      console.log("Success : " + String(chunk));
      require('fs').writeFileSync('ip.js',"var ip=\""+String(chunk)+"\";");
    });
    res.on('error', function(err) {
      console.log("Error : " + err);
    });
  });

req.end();
