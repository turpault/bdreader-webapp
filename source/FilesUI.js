/*jslint browser: true, indent: 2 */
/*global enyo */

enyo.kind({
  name: "filesUI",
  kind: "FittableRows",
  fit: true,
  narrowFit: false,
  classes: "onyx",
  components: [
    {
      kind: "onyx.MoreToolbar",
      name: "toolbar",
      components: [
        {content: "", name: "title"},
        {content: "", name: "path", fit: true},
        {
          kind: 'onyx.Button',
          ontap: 'refresh',
          content: 'Refresh',
          name: "refresh"
        }, {
          kind: 'onyx.Button',
          ontap: "back",
          content: 'Back',
          name: "back",
          unmoveable: true
        }
      ]
    }, {
      kind: "Panels",
      fit: true,
      name: 'pane',
      arrangerKind: "SladeArranger",
      margin: 0,
      handlers: {
        onTransitionFinish: "onTransitionFinish"
      },
      components: [
        /*{
          kind:'FittableRows',
          noStretch: false,
          style:'width:100%',
          path:'',
          components:[
            {content:'Please select the repository'},
            {kind:'onyx.Button', style:'width:100%;margin-top:8px;', content:'Files on Server', ontap:'openServer'},
            {kind:'onyx.Button', style:'width:100%;margin-top:8px;', content:'Recent files on server', ontap:'openRecent'},
            {kind:'onyx.Button', name:'downloadedBtn', style:'width:100%;margin-top:8px', content:'Downloaded files', ontap:'openDownloaded'},
            {kind:'onyx.Button', name:'favoritesBtn', style:'width:100%;margin-top:8px', content:'Favorite list', ontap:'openFavorites'},
            {name: "divider", classes: "file-list-divider", style:'margin-top:8px',},
            {kind:'onyx.Button', style:'width:100%;margin-top:8px', content:'Settings', ontap:'openSettings'},
            {kind:'onyx.Button', style:'width:100%;margin-top:8px', content:'Feedback', ontap:'openFeedback'}
          ]
        }*/
      ],
      onTransitionFinish: function (inSender, inEvent) {
        this.parent.updateToolbar();
      }
            
      //style:"background:green"
    }  
  ],
  handlers: {
    onFolderSelected: "onFolderSelected" 
  },
  events: {
    onReturnToMain: "",
    onOpenPanel: "",
    onAskFeedback: "",
    onNotification: ""
  },
  create: function(options) {
    this.inherited(arguments);    
  },
  onShow: function() {
    this.updateToolbar();
  },
  back: function() {
    this.$.pane.previous();
  },  
  updateToolbar: function() {
    if(this.$.pane.getActive()) {
      this.$.path.setContent(this.$.pane.getActive().path);    
      this.$.back.setShowing(this.$.pane.getIndex()>0);
//    this.$.refresh.setShowing(!(inSender && inSender.hideRefresh) && this.$.pane.getIndex()>0);
    }
    this.$.toolbar.reflow();
  },  
  destroyPanesStartingAt: function(index) {
    var panelsToDestroy=this.$.pane.getPanels().slice(index);
    panelsToDestroy.forEach(function(panel) {panel.destroy();});
  },
  openServer: function() {
    this.$.title.setContent("Server");
    this.destroyPanesStartingAt(1);
    this.openFolder(MODEL.config.initialPath);
  },    
  openRecent:function() {
    this.$.title.setContent("Recent");
    this.destroyPanesStartingAt(1);
    var that = this;
    MODEL.getRecentFiles(function(err, results) {
      if(err) { 
        console.log("error - displaying reason");
      } else {    
        console.log("Got " + results.data.length + " recent files");
        var page = that.$.pane.createComponent({kind:"files", hideRefresh:true},{index:that.$.pane.getPanels().length});      
        page.render();

        page.renderList(results.data);
        page.reflow();
        that.$.pane.setIndex(that.$.pane.getPanels().length-1);
      }
    });
  } ,
  openDownloaded:function() {
    this.$.title.setContent("Local");
    this.destroyPanesStartingAt(1);
    var that = this;
    MODEL.getLocalFiles(function(err, results) {
      if(err) { 
      	console.log("error - displaying reason");
      } else {		
				var page = that.$.pane.createComponent({kind:"files", hideRefresh:true},{index:that.$.pane.getPanels().length});      
				page.render();
				page.renderList(results);
				page.reflow();
				that.$.pane.setIndex(that.$.pane.getPanels().length-1);
			}
    });
  }  ,  
  openFavorites:function() {
    this.$.title.setContent("Favorites");
    this.destroyPanesStartingAt(1);
    var that = this;
    MODEL.getStarredFiles(function(err, results) {
      if(err) { 
        console.log("error - displaying reason");
      } else {    
        var page = that.$.pane.createComponent({kind:"files", hideRefresh:true},{index:that.$.pane.getPanels().length});      
        page.render();
        page.renderList(results);
        page.reflow();
        that.$.pane.setIndex(that.$.pane.getPanels().length-1);
      }
    });
  }  ,  
  openSettings:function() {
    this.doOpenPanel({panel:"settings"});
  },
  popup:function(text) {
    var popup = this.createComponent({
      kind: "onyx.Popup", 
      centered: true, 
      modal: true, 
      floating: true, 
      handlers:{
        onHide:"hidePopup"
      },
      hidePopup:function() {console.log('Hiding'); this.destroy();},
      components: [
        {content:text}
      ]});
    popup.render();
    popup.show();
  },
  openFeedback:function() {
    this.doAskFeedback();
  },
  submitFeedback:function(inSender, inEvent) {
    console.log("Sending feedback");
    this.$.feedbackPopup.hide();
  },
  refresh:function() {
    var panel =this.$.pane.getActive(); 
    var path=panel.path;
    MODEL.getFilesAndFolders(path, function(err, results) {
      if(!err) panel.renderList(results.data);
    });
  },
  openFolder:function(path) {
    var that = this;
    MODEL.getFilesAndFolders(path, function(err, results) {
      console.log("request results: " + err);
      if(err) { 
        that.doNotification(err);
        page.render();
        page.reflow();
      } else {    
        var page = that.$.pane.createComponent({kind:"filegrid"},{index:that.$.pane.getPanels().length, path:path});      
        page.render();

        page.renderList(results.data);
        page.reflow();
        that.$.pane.setIndex(that.$.pane.getPanels().length-1);
        that.updateToolbar();
      }
      //that.$.pane.reflow();
    });
  },
  onFolderSelected:function(inSender, params) {
    console.log("Selected folder" + params.folder.path);
    this.destroyPanesStartingAt(params.index+1);
    this.openFolder(params.folder.path);
  },

  close:function() { 
    this.doReturnToMain();
  },  
  onShowPanel:function(inSender, inEvent) {
    this.doOpenPanel({panel:inSender.name});
  }
  
});
